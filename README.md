# Houston configuration
This is the application to check the Houston configurations, check the current configuration values, and change the values easily.

The application reads the config-descriptor.yml files to get the information about configurations.
It also reads the javaconfig.properties files in the Houston configuration to get the current values.

The application helps the user to get the configuration. The user can search the configuration value or description.
The user can change the Houston configuration value using the UI of this application.
If the configuration contains the list of possible values, the user can select the value from the select list.
If the configuration defines the pattern, this application checks the pattern in the real time when changing the value.