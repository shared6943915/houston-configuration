# Change to the directory containing pom.xml
Set-Location "../"

# Run Maven commands
& 'mvn' 'clean', 'validate', 'compile', 'test', 'package'

# Define relative source and destination paths
$sourceFile = "target/Houston_configuration.jar"
$sourceFolder = "target/lib"
$destFile = "applicationBuild/Houston_configuration.jar"
$destFolder = "applicationBuild/lib"

# Check if destination file and folder exist, if so delete them
if(Test-Path -Path $destFile)
{
    Remove-Item -Path $destFile
}

if(Test-Path -Path $destFolder)
{
    Remove-Item -Path $destFolder -Recurse
}

# Copy source file and folder to destination
Copy-Item -Path $sourceFile -Destination $destFile
Copy-Item -Path $sourceFolder -Destination $destFolder -Recurse