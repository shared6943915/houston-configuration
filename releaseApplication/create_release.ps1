Param(
    [Parameter(Mandatory=$true)][string]$nextVersion
)

# Path to the pom.xml file
$pomPath = Join-Path -Path (Split-Path $PSScriptRoot -Parent) -ChildPath "pom.xml"

# Load the content of the pom.xml file
[xml]$pomFile = Get-Content $pomPath

# Create namespace manager
$nsManager = New-Object System.Xml.XmlNamespaceManager($pomFile.NameTable)
$nsManager.AddNamespace("ns", "http://maven.apache.org/POM/4.0.0")

# Select the version node
$versionNode = $pomFile.SelectSingleNode('//ns:project/ns:version', $nsManager)

# Check if version node is found and change the version
if($null -ne $versionNode)
{
    $versionNode.InnerText = $nextVersion
}
else
{
    Write-Host "Version node not found"
}

# Save changes on pom.xml
$pomFile.Save($pomPath)


# Create a new branch with the name format release/VERSION
$branchName = "release/$nextVersion"
git checkout -b $branchName
