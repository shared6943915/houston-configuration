# Create the release
1. Run the script "next_release.bat". This script will checkout the DEVELOP branch, update the branch, ask for the new version, change the version in pom.xml file, extend the CHANGELOG.md file, create and checkout the branch "release/XXX" and it will execute "build_and_copy_target.ps1" script
2. Check if the CHANGELOG.md file is complete
3. Commit with the message "chore: release XXX". **DO NOT PUSH!!!**
4. Merge the MASTER branch and resolve conflicts. **FORCE PUSH**
5. Create the merge request to the MASTER branch. **DO NOT SQUASH COMMITS**
6. Merge the merge request
7. Create release and tag in Git
