package com.madera.michal.houstonconfiguration.logger;

/**
 * The LogType enum represents different types of log messages.
 */
public enum LogType {
    INFO("INFO"),
    WARN("WARN"),
    ERROR("ERROR");

    private final String logPrefix;

    LogType(final String logPrefix) {
        this.logPrefix = logPrefix;
    }

    public String getLogPrefix() {
        return logPrefix;
    }

}
