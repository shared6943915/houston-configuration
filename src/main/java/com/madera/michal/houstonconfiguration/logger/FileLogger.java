package com.madera.michal.houstonconfiguration.logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * The FileLogger class is responsible for logging messages to a file.
 * It implements the ILogger interface and extends the LoggerMessageCreator class.
 */
public class FileLogger extends LoggerMessageCreator implements ILogger {
    private final String fileName;

    FileLogger(final String className) {
        super(className);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        this.fileName = "logs" + File.separator + "server_" + formatter.format(LocalDate.now()) + ".log";

        File folder = new File("logs");
        if (!folder.exists()) {
            folder.mkdirs();
        }
    }

    /**
     * JUST FOR TESTS!!!
     */
    FileLogger(final String className, final String fileName) {
        super(className);
        this.fileName = fileName;
    }

    @Override
    public void info(final String message, final Object... args) {
        saveLogMessage(LogType.INFO, message, args);
    }

    @Override
    public void warn(final String message, final Object... args) {
        saveLogMessage(LogType.WARN, message, args);
    }

    @Override
    public void error(final String message, final Object... args) {
        saveLogMessage(LogType.ERROR, message, args);
    }

    private void saveLogMessage(final LogType type, final String message, final Object... messageArguments) {
        try {
            String logMessage = createLogMessage(type, message, messageArguments);

            FileWriter fileWriter = new FileWriter(fileName, true);
            PrintWriter writer = new PrintWriter(fileWriter);
            writer.println(logMessage);
            writer.close();
        } catch (IOException e) {
            // Should not happen...
        }
    }

}
