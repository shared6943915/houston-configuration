package com.madera.michal.houstonconfiguration.logger;

/**
 * ILogger is an interface that provides methods for logging different levels of messages.
 * Implementing classes should provide logic to handle the logging of messages.
 */
public interface ILogger {
    /**
     * Logs an informational message with optional arguments.
     *
     * @param message the message to log
     * @param args optional arguments to be formatted into the message
     */
    void info(String message, Object... args);

    /**
     * Logs a warning message with optional arguments.
     *
     * @param message the message to log
     * @param args optional arguments to be formatted into the message
     */
    void warn(String message, Object... args);

    /**
     * Logs an error message with optional arguments.
     *
     * @param message the error message to log
     * @param args optional arguments to be formatted into the message
     */
    void error(String message, Object... args);

}
