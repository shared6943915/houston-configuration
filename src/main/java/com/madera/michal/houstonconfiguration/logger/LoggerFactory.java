package com.madera.michal.houstonconfiguration.logger;

import com.madera.michal.houstonconfiguration.configuration.ConfigurationKey;
import com.madera.michal.houstonconfiguration.configuration.ConfigurationProducer;

/**
 * The LoggerFactory class is responsible for creating instances of ILogger.
 * It provides a static method to create a logger based on the given class.
 */
public class LoggerFactory {
    private LoggerFactory() {}

    public static ILogger createLogger(final Class<?> clazz) {
        String config = ConfigurationProducer.getInstance().getConfigValue(ConfigurationKey.LOGGER_METHOD);
        if (config != null && config.equals("FILE")) {
            return new FileLogger(clazz.getName());
        }

        return new ConsoleLogger(clazz.getName());
    }

}
