package com.madera.michal.houstonconfiguration.logger;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * The LoggerMessageCreator class is responsible for creating log messages with specific formatting.
 */
public class LoggerMessageCreator {
    private final DateTimeFormatter logTimeFormatter;
    private final String className;

    LoggerMessageCreator(final String className) {
        this.logTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        this.className = className;
    }

    String createLogMessage(final LogType type, final String message, final Object... messageArguments) {
        String messageWithArguments;
        if (messageArguments.length == 0) {
            messageWithArguments = message;
        } else {
            String newMessage = message;
            for (int i = 0; i < messageArguments.length; ++i) {
                newMessage = newMessage.replaceFirst("\\{}", "{" + i + "}");
            }
            messageWithArguments = java.text.MessageFormat.format(newMessage, messageArguments);
        }

        StringBuilder logMessage = new StringBuilder();
        logMessage.append(logTimeFormatter.format(LocalDateTime.now()));
        logMessage.append(" ").append(String.format("%5s", type.getLogPrefix()));
        logMessage.append(" [").append(String.format("%70s", className)).append("]");
        logMessage.append(": ").append(messageWithArguments);

        return logMessage.toString();
    }

}
