package com.madera.michal.houstonconfiguration.ui.mainscreen.controller;

import com.madera.michal.houstonconfiguration.logger.ILogger;
import com.madera.michal.houstonconfiguration.logger.LoggerFactory;
import com.madera.michal.houstonconfiguration.ui.mainscreen.model.MainScreenModel;
import com.madera.michal.houstonconfiguration.worker.ConfigurationDTO;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.util.List;

/**
 * This class represents a controller for managing configuration table.
 */
public class ConfigurationTableController implements IObserver {
    private static final ILogger LOG = LoggerFactory.createLogger(ConfigurationTableController.class);

    private final JPanel panel = new JPanel(new BorderLayout());
    private final DefaultTableModel tableModel;

    private final List<ConfigurationDTO> configurationDtoList;
    private final MainScreenModel model;

    private static final String[] TABLE_COLUMNS = new String[]{"Key", "Documentation", "Type",
            "Pattern", "Possible values", "Default value", "Default value list", "Current value"
    };

    public ConfigurationTableController(final MainScreenModel model) {
        this.model = model;
        this.model.registerObserver(this);
        JPanel filterPanel = new JPanel();

        this.tableModel = new DefaultTableModel(TABLE_COLUMNS, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        JTable table = new JTable(this.tableModel);

        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>(tableModel);
        table.setRowSorter(sorter);

        filterPanel.add(new JLabel("Filter configurations:"));
        filterPanel.add(createKeyFilter(sorter));

        panel.add(filterPanel, BorderLayout.NORTH);
        panel.add(new JScrollPane(table), BorderLayout.CENTER);

        setDoubleClickMouseListener(table);

        configurationDtoList = new LinkedList<>();
    }

    private JTextField createKeyFilter(final TableRowSorter<DefaultTableModel> sorter) {
        JTextField txtFilterKey = new JTextField(20);
        txtFilterKey.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                setRowFilter(txtFilterKey, sorter);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                setRowFilter(txtFilterKey, sorter);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                //...
            }
        });

        return txtFilterKey;
    }

    private void setRowFilter(final JTextField textField, final TableRowSorter<DefaultTableModel> sorter) {
        String text = textField.getText();
        if (text.trim().isEmpty()) {
            sorter.setRowFilter(null);
        } else {
            sorter.setRowFilter(RowFilter.regexFilter("(?i)" + text, 0, 1));
        }
    }

    private void setDoubleClickMouseListener(final JTable table) {
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(final MouseEvent mouseEvent) {
                JTable mouseEventTable = (JTable) mouseEvent.getSource();
                Point point = mouseEvent.getPoint();
                int row = mouseEventTable.rowAtPoint(point);
                if (mouseEvent.getClickCount() == 2 && mouseEventTable.getSelectedRow() != -1) {
                    ConfigurationDTO configuration = configurationDtoList.get(mouseEventTable.convertRowIndexToModel(row));
                    LOG.info("Show details of configuration {}", configuration);
                    ConfigurationDetailsModalController detailsDialog = new ConfigurationDetailsModalController(configuration, model);
                    detailsDialog.showDialog();
                }
            }
        });
    }

    public JPanel getPanel() {
        return panel;
    }

    public void addConfigurationToTable(final ConfigurationDTO configuration) {
        tableModel.addRow(configuration.getTableRowData());
        configurationDtoList.add(configuration);
    }

    @Override
    public void refresh() {
        LOG.info("Refresh the table");
        tableModel.setRowCount(0);
        configurationDtoList.clear();
        model.getConfigurations().forEach(this::addConfigurationToTable);
        LOG.info("Table refreshed");
    }
}
