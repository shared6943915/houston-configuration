package com.madera.michal.houstonconfiguration.ui.mainscreen.model;

public class HoustonConfigPathNotSetException extends Exception {
    public HoustonConfigPathNotSetException() {
        super("Path to the Houston config root directory not set");
    }
}
