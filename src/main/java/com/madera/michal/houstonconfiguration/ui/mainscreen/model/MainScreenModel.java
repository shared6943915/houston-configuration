package com.madera.michal.houstonconfiguration.ui.mainscreen.model;

import com.madera.michal.houstonconfiguration.configuration.ConfigurationKey;
import com.madera.michal.houstonconfiguration.configuration.ConfigurationProducer;
import com.madera.michal.houstonconfiguration.logger.ILogger;
import com.madera.michal.houstonconfiguration.logger.LoggerFactory;
import com.madera.michal.houstonconfiguration.ui.mainscreen.controller.IObserver;
import com.madera.michal.houstonconfiguration.worker.ConfigDescriptorReader;
import com.madera.michal.houstonconfiguration.worker.ConfigurationDTO;
import com.madera.michal.houstonconfiguration.worker.ConfigurationMapKey;
import com.madera.michal.houstonconfiguration.worker.HoustonConfigurationReader;
import com.madera.michal.houstonconfiguration.worker.ModulePathReader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * This class represents the model for the MainScreen.
 * It contains the logic and data required for the MainScreen view.
 */
public class MainScreenModel {
    private static final String CONFIG_DESCRIPTOR_RELATIVE_PATH = File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "config-descriptor.yml";
    private static final ILogger LOG = LoggerFactory.createLogger(MainScreenModel.class);
    private static final String JAVA_CONFIG_FILE = "javaconfig.properties";

    private final ModulePathReader modulePathReader;
    private final ConfigDescriptorReader configDescriptorReader;
    private final HoustonConfigurationReader houstonConfigurationReader;
    private final List<String> modulePaths;
    private String rootHoustonConfigPath;
    private String middlewarePath;
    private Map<String, Properties> currentValues;
    private List<Map<ConfigurationMapKey, ConfigurationDTO>> allConfigurations;
    private final List<IObserver> observers;
    private final ConfigurationProducer configurationProducer;

    public MainScreenModel() throws IOException {
        this.modulePathReader = ModulePathReader.getInstance();
        this.configDescriptorReader = new ConfigDescriptorReader();
        this.houstonConfigurationReader = new HoustonConfigurationReader();
        this.modulePaths = new LinkedList<>();
        this.observers = new LinkedList<>();

        configurationProducer = ConfigurationProducer.getInstance();
        updateMiddlewareAndHoustonConfigPaths();
    }

    private void updateMiddlewareAndHoustonConfigPaths() throws IOException {
        updateModulePaths();
        updateRootHoustonConfigPath();
    }

    private void updateModulePaths() throws IOException {
        String middlewarePathFromConfig = configurationProducer.getConfigValue(ConfigurationKey.MIDDLEWARE_PATH);
        if (middlewarePathFromConfig != null) {
            createModulePaths(middlewarePathFromConfig);
        }
    }

    private void updateRootHoustonConfigPath() throws IOException {
        String houstonPathFromConfig = configurationProducer.getConfigValue(ConfigurationKey.HOUSTON_CONFIG_PATH);
        if (houstonPathFromConfig != null) {
            setRootHoustonConfigPath(houstonPathFromConfig);
        }
    }

    public void registerObserver(final IObserver observer) {
        observers.add(observer);
    }

    public void savePathsToConfiguration() throws IOException {
        configurationProducer.setConfigValue(ConfigurationKey.MIDDLEWARE_PATH, middlewarePath);
        configurationProducer.setConfigValue(ConfigurationKey.HOUSTON_CONFIG_PATH, rootHoustonConfigPath);
    }

    public List<String> getModulePaths() {
        return modulePaths;
    }

    public void createModulePaths(final String middlewareMainPath) throws IOException {
        LOG.info("Create module paths from main path: {}", middlewareMainPath);
        this.middlewarePath = middlewareMainPath;
        modulePaths.addAll(modulePathReader.getModulePaths(middlewareMainPath));
        LOG.info("Module paths created");
    }

    public String getRootHoustonConfigPath() {
        return rootHoustonConfigPath;
    }

    public void setRootHoustonConfigPath(final String rootHoustonConfigPath) throws IOException {
        LOG.info("Set root Houston config path to {}", rootHoustonConfigPath);
        this.rootHoustonConfigPath = rootHoustonConfigPath;
        this.currentValues = houstonConfigurationReader.readHoustonConfigurations(rootHoustonConfigPath);
        LOG.info("Root Houston config path updated");
    }

    public void loadConfigurations() throws IOException, ModulePathsNotSetException, HoustonConfigPathNotSetException {
        LOG.info("Load configurations");
        allConfigurations = new LinkedList<>();
        checkPreconditionsForLoadConfigurations();
        loadEachConfiguration();
        LOG.info("Loaded {} configuration(s)", allConfigurations.size());
    }

    private void checkPreconditionsForLoadConfigurations() throws ModulePathsNotSetException, HoustonConfigPathNotSetException {
        if (areModulePathsEmpty()) {
            LOG.warn("Module paths not set");
            throw new ModulePathsNotSetException();
        }
        if (isHoustonConfigPathNotSet()) {
            LOG.warn("Houston config path not set");
            throw new HoustonConfigPathNotSetException();
        }
    }

    private boolean areModulePathsEmpty() {
        return modulePaths.isEmpty();
    }

    private boolean isHoustonConfigPathNotSet() {
        return rootHoustonConfigPath == null;
    }

    private void loadEachConfiguration() throws IOException {
        List<String> configDescriptorPaths = modulePaths
                .stream()
                .map(path -> path + CONFIG_DESCRIPTOR_RELATIVE_PATH)
                .toList();
        for (String configPath : configDescriptorPaths) {
            Map<ConfigurationMapKey, ConfigurationDTO> configurations = configDescriptorReader.getConfigurations(configPath);
            setCurrentConfigurationValue(configurations, currentValues);
            allConfigurations.add(configurations);
        }
    }

    public List<ConfigurationDTO> getConfigurations() {
        return allConfigurations.stream()
                .flatMap(map -> map.entrySet().stream())
                .map(Map.Entry::getValue)
                .toList();
    }

    private void setCurrentConfigurationValue(
            final Map<ConfigurationMapKey, ConfigurationDTO> configurations,
            Map<String, Properties> currentValues
    ) {
        LOG.info("Set current configuration values");
        for (Map.Entry<ConfigurationMapKey, ConfigurationDTO> entry : configurations.entrySet()) {
            ConfigurationMapKey key = entry.getKey();
            ConfigurationDTO value = entry.getValue();
            Properties currentValue = currentValues.get(key.moduleName());

            value.setCurrentValue(currentValue.get(key.key()).toString());
        }
        LOG.info("Current configuration values set");
    }

    public void changeConfigurationValue(final ConfigurationDTO configuration, final String newValue) throws IOException {
        LOG.info("Change value for {} to {}", configuration, newValue);
        configuration.setCurrentValue(newValue);
        Properties properties = currentValues.get(configuration.getModuleName());
        String configFilePath = rootHoustonConfigPath + File.separator + configuration.getModuleName() + File.separator + JAVA_CONFIG_FILE;
        properties.setProperty(configuration.getKey(), newValue);
        try (FileOutputStream fos = new FileOutputStream(configFilePath)) {
            properties.store(fos, null);
        }

        observers.forEach(IObserver::refresh);
        LOG.info("Value changed");
    }
}
