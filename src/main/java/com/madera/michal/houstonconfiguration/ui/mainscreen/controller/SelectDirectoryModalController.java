package com.madera.michal.houstonconfiguration.ui.mainscreen.controller;

import com.madera.michal.houstonconfiguration.logger.ILogger;
import com.madera.michal.houstonconfiguration.logger.LoggerFactory;
import com.madera.michal.houstonconfiguration.ui.mainscreen.model.MainScreenModel;
import org.apache.commons.lang3.exception.ExceptionUtils;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * The SelectDirectoryDialogController class is responsible for controlling the select directory dialog.
 */
public class SelectDirectoryModalController extends AbstractModalController {
    private static final String SELECT_MIDDLEWARE_TEXT = "You did not select the Middleware directory.\n For the correct behavior of application, you need to select the directory with Middleware modules.\nThe application analyzes the modules and shows all Houston configuration properties.";
    private static final String SELECT_HOUSTON_TEXT = "You did not select the directory with Houston configs.\n For the correct behavior or application, you need to select the directory with current Houston configuration values.";
    private static final Integer DIALOG_WIDTH = 500;
    private static final Integer DIALOG_HEIGHT = 200;
    private static final Integer TEXT_AREA_WIDTH = DIALOG_WIDTH - 20;
    private static final Integer TEXT_AREA_HEIGHT = DIALOG_HEIGHT - 80;
    private static final ILogger LOG = LoggerFactory.createLogger(SelectDirectoryModalController.class);

    private JTextArea textArea;

    private final MainScreenModel model;
    private boolean middlewareDirectory;

    public SelectDirectoryModalController(final MainScreenModel model, final boolean middlewareDirectory) {
        this.model = model;
        this.middlewareDirectory = middlewareDirectory;
        LOG.info("Select directory. Middleware directory?: {}", middlewareDirectory);
        prepareDialog();
    }

    private void prepareDialog() {
        createDialog("Warning", DIALOG_WIDTH, DIALOG_HEIGHT, "Select directory", e -> selectDirectory(), createContentPanel());
    }

    private JPanel createContentPanel() {
        JPanel contentPanel = new JPanel();

        textArea = new JTextArea();
        textArea.setPreferredSize(new Dimension(TEXT_AREA_WIDTH, TEXT_AREA_HEIGHT));
        textArea.setEditable(false);
        textArea.setText(middlewareDirectory ? SELECT_MIDDLEWARE_TEXT : SELECT_HOUSTON_TEXT);
        textArea.setLineWrap(true);

        contentPanel.add(textArea);

        return contentPanel;
    }

    private void selectDirectory() {
        try {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

            int returnValue = fileChooser.showOpenDialog(null);

            if (returnValue == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                if (middlewareDirectory) {
                    model.createModulePaths(selectedFile.getAbsolutePath());
                } else {
                    model.setRootHoustonConfigPath(selectedFile.getAbsolutePath());
                }
                LOG.info("Directory selected");

                dialog.setVisible(false);
            }
        } catch (IOException e) {
            LOG.error("Error occurred when selecting the directory: {}", ExceptionUtils.getStackTrace(e));
        }
    }

    public void setMiddlewareDirectory(final boolean middlewareDirectory) {
        this.middlewareDirectory = middlewareDirectory;
        textArea.setText(middlewareDirectory ? SELECT_MIDDLEWARE_TEXT : SELECT_HOUSTON_TEXT);
    }
}
