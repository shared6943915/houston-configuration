package com.madera.michal.houstonconfiguration.ui.mainscreen.controller;

import com.madera.michal.houstonconfiguration.logger.ILogger;
import com.madera.michal.houstonconfiguration.logger.LoggerFactory;
import com.madera.michal.houstonconfiguration.worker.ConfigurationDTO;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * The SetListConfigurationModalController class represents a controller for a modal dialog used to set a list of values.
 * It allows adding, removing, and saving the values.
 */
public class SetListConfigurationModalController extends AbstractModalController {
    private static final Integer DIALOG_WIDTH = 900;
    private static final Integer DIALOG_HEIGHT = 500;
    private static final String LIST_VALUE_DELIMITER = ";";
    private static final ILogger LOG = LoggerFactory.createLogger(SetListConfigurationModalController.class);

    private JTable valuesTable;
    private DefaultTableModel valuesTableModel;
    private final ConfigurationDTO configuration;
    private final List<String> currentValue;

    public SetListConfigurationModalController(final ConfigurationDTO configuration) {
        this.configuration = configuration;
        this.currentValue = Arrays.stream(configuration.getCurrentValue()
                .split(LIST_VALUE_DELIMITER))
                .filter(value -> !value.isEmpty())
                .collect(Collectors.toList());
        prepareDialog();
    }

    private void prepareDialog() {
        createDialog("Set list of values", DIALOG_WIDTH, DIALOG_HEIGHT, "Save", e -> dialog.setVisible(false), createContentPanel());
    }

    private JPanel createContentPanel() {
        JPanel contentPanel = new JPanel();

        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
        addCurrentValue(contentPanel);

        return contentPanel;
    }

    private void addCurrentValue(final JPanel contentPanel) {
        JLabel titleLbl = new JLabel("<html><b>Current value:</b></html>");

        valuesTableModel = new DefaultTableModel(new String[] {"Value"}, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        valuesTable = new JTable(valuesTableModel);
        currentValue.forEach(value -> valuesTableModel.addRow(new String[]{value}));

        JPanel rowPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JButton addButton = new JButton("Add");
        addButton.addActionListener(e -> addNewValue());

        JButton removeButton = new JButton("Remove");
        removeButton.addActionListener(e -> removeSelectedValue());

        rowPanel.add(addButton);
        rowPanel.add(removeButton);

        contentPanel.add(titleLbl);
        contentPanel.add(valuesTable);
        contentPanel.add(rowPanel);
    }

    private void addNewValue() {
        LOG.info("Add new value");
        JDialog addValueDialog = new JDialog();
        setUpAddValueDialog(addValueDialog);
        JButton saveNewValueButton = new JButton("Add");
        JPanel contentPanel = new JPanel();
        saveNewValueButton.addActionListener(e -> hideAddValueDialog(addValueDialog));

        if (configuration.getPossibleValues() != null && !configuration.getPossibleValues().isEmpty()) {
            String[] availableValues = filterCurrentValueFromPossibleValues();
            comboBoxAddValue(contentPanel, availableValues);
        } else {
            textFieldAddValue(contentPanel, saveNewValueButton);
        }

        completeAddValueDialogSetup(addValueDialog, contentPanel, saveNewValueButton);
    }

    private String[] filterCurrentValueFromPossibleValues() {
        return configuration.getPossibleValues()
                .stream()
                .filter(value -> !currentValue.contains(value))
                .toArray(String[]::new);
    }

    private void comboBoxAddValue(final JPanel contentPanel, final String[] availableValues) {
        JComboBox<String> comboBox = new JComboBox<>(availableValues);
        comboBox.setSelectedItem(configuration.getCurrentValue());
        comboBox.addActionListener(e -> currentValue.add(Objects.requireNonNull(comboBox.getSelectedItem()).toString()));
        contentPanel.add(comboBox);
    }

    private void textFieldAddValue(final JPanel contentPanel, final JButton saveNewValueButton) {
        JTextField textField = new JTextField();
        setUpTextField(textField, saveNewValueButton, configuration.getPattern());
        JLabel lblInstructions = new JLabel("<html><b>To add the value, you need to press Enter and then click on Add button!</b></html>");
        contentPanel.add(lblInstructions);
        contentPanel.add(textField);
    }

    private void setUpTextField(final JTextField textField, final JButton saveNewValueButton, final String pattern) {
        textField.setPreferredSize(PREFERRED_TEXT_FIELD_SIZE);
        textField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void changedUpdate(DocumentEvent e) {
                saveCurrentValueAndDisableIfNotMatch();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                saveCurrentValueAndDisableIfNotMatch();
            }
            @Override
            public void insertUpdate(DocumentEvent e) {
                saveCurrentValueAndDisableIfNotMatch();
            }
            private void saveCurrentValueAndDisableIfNotMatch() {
                if (pattern != null) {
                    saveNewValueButton.setEnabled(textField.getText().matches(pattern));
                }
            }
        });
        textField.addActionListener(e -> updateCurrentValue(textField.getText()));
    }

    private void updateCurrentValue(final String value) {
        if (configuration.getPattern() == null ||
                (configuration.getPattern() != null && value.matches(configuration.getPattern()))
        ) {
            LOG.info("Adding new value: {}", value);
            currentValue.add(value);
        }
    }

    private void setUpAddValueDialog(final JDialog addValueDialog) {
        addValueDialog.setTitle("Add new value to list");
        addValueDialog.setSize(500, 200);
        addValueDialog.setResizable(false);
        addValueDialog.setLocationRelativeTo(dialog);
        addValueDialog.setModal(true);
    }

    private void hideAddValueDialog(final JDialog addValueDialog) {
        addValueDialog.setVisible(false);
        LOG.info("Value added");
    }

    private void completeAddValueDialogSetup(final JDialog addValueDialog, final JPanel contentPanel, final JButton saveNewValueButton) {
        addValueDialog.add(contentPanel);
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(saveNewValueButton);
        addValueDialog.add(buttonPanel, BorderLayout.SOUTH);
        addValueDialog.setVisible(true);

        valuesTableModel.setRowCount(0);
        currentValue.forEach(value -> valuesTableModel.addRow(new String[]{value}));
    }

    private void removeSelectedValue() {
        LOG.info("Remove selected row");
        int selectedRow = valuesTable.getSelectedRow();
        if (selectedRow >= 0) {
            valuesTableModel.removeRow(selectedRow);
            currentValue.remove(selectedRow);
        }
        LOG.info("Selected row removed");
    }

    public String getCurrentValueAsString() {
        return String.join(LIST_VALUE_DELIMITER, currentValue);
    }
}
