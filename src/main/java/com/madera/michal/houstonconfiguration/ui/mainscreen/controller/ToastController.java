package com.madera.michal.houstonconfiguration.ui.mainscreen.controller;

import com.madera.michal.houstonconfiguration.logger.ILogger;
import com.madera.michal.houstonconfiguration.logger.LoggerFactory;
import org.apache.commons.lang3.exception.ExceptionUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;

/**
 * The ToastController class is responsible for managing toast messages.
 * It provides methods for displaying toast messages with different durations
 * and customizing the appearance of the toast messages.
 */
public class ToastController {
    public static final Integer TOAST_LENGTH_SHORT = 1500;
    public static final Integer TOAST_LENGTH_MEDIUM = 3000;
    public static final Integer TOAST_LENGTH_LONG = 5000;
    private static final ILogger LOG = LoggerFactory.createLogger(ToastController.class);

    private ToastController() {}

    public static void showToast(final Window window, final String toastText) {
        showToast(window, toastText, TOAST_LENGTH_MEDIUM);
    }

    public static void showToast(final Window window, final String toastText, final Integer duration) {
        LOG.info("Show toast with text `{}` and duration {}", toastText, duration);
        JDialog toast = new JDialog(window);
        toast.setSize(300, 50);
        toast.setUndecorated(true);
        toast.setLayout(new BorderLayout(0, 0));

        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        panel.setBackground(Color.BLACK);

        JLabel toastLabel = new JLabel();
        toastLabel.setText(toastText);
        toastLabel.setForeground(Color.WHITE);

        panel.add(toastLabel);
        toast.add(panel, BorderLayout.CENTER);

        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gd = ge.getDefaultScreenDevice();
        final boolean isTranslucencySupported = gd.isWindowTranslucencySupported(GraphicsDevice.WindowTranslucency.TRANSLUCENT);

        if (!gd.isWindowTranslucencySupported(GraphicsDevice.WindowTranslucency.PERPIXEL_TRANSPARENT)) {
            LOG.error("Shaped windows are not supported");
        }

        if (!isTranslucencySupported) {
            LOG.warn("Translucency is not supported");
        }

        toast.setShape(new RoundRectangle2D.Double(10, 10, toast.getWidth() - 20.0, toast.getHeight() - 20.0, 15, 15));
        toast.setOpacity(0.5f);
        toast.setLocationRelativeTo(window);
        toast.setVisible(true);

        new Thread(() -> {
            try {
                Thread.sleep(duration); // Duration of how long the toast should stay visible
            } catch (InterruptedException e) {
                LOG.error("Error when showing Toast: {}", ExceptionUtils.getStackTrace(e));

                Thread.currentThread().interrupt();
            }
            toast.dispose();
        }).start();
    }
}
