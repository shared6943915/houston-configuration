package com.madera.michal.houstonconfiguration.ui.mainscreen.model;

/**
 * ModulePathsNotSetException is an exception class that is thrown when the path to the modules is not set.
 */
public class ModulePathsNotSetException extends Exception {
    public ModulePathsNotSetException() {
        super("Path to the modules is not set");
    }
}
