package com.madera.michal.houstonconfiguration.ui.mainscreen.controller;

import com.madera.michal.houstonconfiguration.logger.ILogger;
import com.madera.michal.houstonconfiguration.logger.LoggerFactory;
import com.madera.michal.houstonconfiguration.ui.mainscreen.model.MainScreenModel;
import com.madera.michal.houstonconfiguration.worker.ConfigurationDTO;
import org.apache.commons.lang3.exception.ExceptionUtils;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The ConfigurationDetailsController class handles the functionality related to configuration details.
 */
public class ConfigurationDetailsModalController extends AbstractModalController {
    private static final Integer DIALOG_WIDTH = 900;
    private static final Integer DIALOG_HEIGHT = 750;
    private static final String LIST_TYPE = "LIST";
    private static final int LABEL_WIDTH = DIALOG_WIDTH - 320;
    private static final ILogger LOG = LoggerFactory.createLogger(ConfigurationDetailsModalController.class);

    private final ConfigurationDTO configuration;
    private String currentValue;
    private final MainScreenModel model;

    public ConfigurationDetailsModalController(final ConfigurationDTO configuration, final MainScreenModel model) {
        super();
        this.configuration = configuration;
        this.model = model;
        this.currentValue = configuration.getCurrentValue();
        prepareDialog();
    }

    private void prepareDialog() {
        ActionListener confirmButtonActionListener = createConfirmButtonActionListener();
        createDialog("Details of " + configuration.getKey(), DIALOG_WIDTH, DIALOG_HEIGHT, "Save", confirmButtonActionListener, createContentPanel());
    }

    private ActionListener createConfirmButtonActionListener() {
        return e -> {
            try {
                LOG.info("Change value of configuration {} to {}", configuration, currentValue);
                model.changeConfigurationValue(configuration, currentValue);
                dialog.setVisible(false);
            } catch (IOException ex) {
                LOG.error("Error occurred when change value of configuration: {}", ExceptionUtils.getStackTrace(ex));
            }
        };
    }

    private JPanel createContentPanel() {
        JPanel contentPanel = new JPanel();
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
        addPropertyRowsToPanel(contentPanel);
        addCurrentValue(contentPanel);
        return contentPanel;
    }

    private void addPropertyRowsToPanel(final JPanel contentPanel) {
        addRowToDialog("Configuration key: ", configuration.getKey(), contentPanel, true);
        addRowToDialog("Documentation: ", configuration.getDocumentation(), contentPanel);
        addRowToDialog("Type: ", configuration.getType(), contentPanel);
        addRowToDialog("Pattern: ", configuration.getPattern(), contentPanel);
        addRowToDialog("Possible values: ", configuration.getPossibleValues(), contentPanel);
        addRowToDialog("Default value: ", configuration.getDefaultValue(), contentPanel);
        addRowToDialog("Default value list: ", configuration.getDefaultValueList(), contentPanel);
    }

    private void addRowToDialog(final String title, final Object value, final JPanel contentPanel) {
        addRowToDialog(title, value, contentPanel, false);
    }

    private void addRowToDialog(final String title, final Object value, final JPanel contentPanel, final boolean copyToClipboard) {
        JLabel titleLbl = new JLabel("<html><b>" + title + "</b></html>");
        JLabel lblValue = new JLabel();
        setLblValueText(lblValue, value);
        Font valueFont = lblValue.getFont().deriveFont(Font.PLAIN);
        lblValue.setFont(valueFont);

        if (copyToClipboard) {
            addCopyToClipboardListenerToLabel(lblValue);
        }

        JPanel rowPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        rowPanel.add(titleLbl);
        rowPanel.add(lblValue);

        contentPanel.add(rowPanel);
    }

    private void addCopyToClipboardListenerToLabel(final JLabel label) {
        label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                if (me.getClickCount() == 2) {
                    StringSelection stringSelection = new StringSelection(getValueFromLabelText(label));
                    Clipboard clipboard = Toolkit.getDefaultToolkit()
                            .getSystemClipboard();
                    clipboard.setContents(stringSelection, null);

                    ToastController.showToast(dialog, "Configuration key copied to clipboard");
                }
            }
        });
    }

    private void addCurrentValue(final JPanel contentPanel) {
        JLabel titleLbl = new JLabel("<html><b>Current value:</b></html>");
        JPanel rowPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        rowPanel.add(titleLbl);
        if (configuration.getType().equals(LIST_TYPE)) {
            addListButton(rowPanel);
        } else if (configuration.getPossibleValues() != null && !configuration.getPossibleValues().isEmpty()) {
            addComboBox(rowPanel);
        } else if (configuration.getPattern() != null) {
            addTextField(rowPanel);
        }

        contentPanel.add(rowPanel);
    }

    private void addListButton(final JPanel contentPanel) {
        JPanel rowPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JTextArea txtCurrentValue = new JTextArea();
        txtCurrentValue.setText(configuration.getCurrentValue());
        txtCurrentValue.setWrapStyleWord(true);
        txtCurrentValue.setOpaque(false);
        txtCurrentValue.setEditable(false);
        Font valueFont = txtCurrentValue.getFont().deriveFont(Font.PLAIN);
        txtCurrentValue.setFont(valueFont);
        JButton listButton = new JButton("Set List");
        listButton.addActionListener(e -> {
            SetListConfigurationModalController setListModal = new SetListConfigurationModalController(configuration);
            setListModal.showDialog();

            String newValue = setListModal.getCurrentValueAsString();
            currentValue = newValue;
            txtCurrentValue.setText(newValue);
        });

        JScrollPane scrollPane = new JScrollPane(txtCurrentValue);
        scrollPane.setPreferredSize(new Dimension(DIALOG_WIDTH - 100, 50));
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        rowPanel.add(scrollPane);

        contentPanel.add(rowPanel);
        contentPanel.add(listButton);
    }

    private void setLblValueText(final JLabel lblValue, final Object value) {
        lblValue.setText("<html><body style='width: " + LABEL_WIDTH + "px'>" + value + "</body></html>");
    }

    private String getValueFromLabelText(final JLabel label) {
        String regex = "<html><body style='width: 580px'>(.*?)</body></html>";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(label.getText());

        if (matcher.find()) {
            return matcher.group(1);
        }

        return label.getText();
    }

    private void addComboBox(final JPanel contentPanel) {
        JComboBox<String> comboBox = new JComboBox<>(configuration.getPossibleValues().toArray(new String[0]));
        comboBox.setSelectedItem(configuration.getCurrentValue());
        comboBox.addActionListener(e -> currentValue = Objects.requireNonNull(comboBox.getSelectedItem()).toString());
        contentPanel.add(comboBox);
    }

    private void addTextField(final JPanel contentPanel) {
        JTextField textField = new JTextField(configuration.getCurrentValue());
        textField.setPreferredSize(PREFERRED_TEXT_FIELD_SIZE);
        textField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void changedUpdate(DocumentEvent e) {
                saveCurrentValueAndDisableIfNotMatch();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                saveCurrentValueAndDisableIfNotMatch();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                saveCurrentValueAndDisableIfNotMatch();
            }

            private void saveCurrentValueAndDisableIfNotMatch() {
                currentValue = textField.getText();
                confirmButton.setEnabled(textField.getText().matches(configuration.getPattern()));
            }
        });
        contentPanel.add(textField);
    }
}
