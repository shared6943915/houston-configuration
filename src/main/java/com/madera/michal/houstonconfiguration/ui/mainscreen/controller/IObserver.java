package com.madera.michal.houstonconfiguration.ui.mainscreen.controller;

/**
 * The IObserver interface represents an observer in the Observer design pattern.
 * It defines a method that must be implemented by any class that wants to act as an observer.
 */
public interface IObserver {
    /**
     * The refresh() method refreshes the state of the object.
     * It should be implemented by any class that wants to act as an observer.
     * This method does not return any value.
     */
    void refresh();
}
