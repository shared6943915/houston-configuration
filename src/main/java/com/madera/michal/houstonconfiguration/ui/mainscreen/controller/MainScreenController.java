package com.madera.michal.houstonconfiguration.ui.mainscreen.controller;

import com.madera.michal.houstonconfiguration.logger.ILogger;
import com.madera.michal.houstonconfiguration.logger.LoggerFactory;
import com.madera.michal.houstonconfiguration.ui.mainscreen.model.HoustonConfigPathNotSetException;
import com.madera.michal.houstonconfiguration.ui.mainscreen.model.MainScreenModel;
import com.madera.michal.houstonconfiguration.ui.mainscreen.model.ModulePathsNotSetException;
import com.madera.michal.houstonconfiguration.worker.ConfigurationDTO;
import org.apache.commons.lang3.exception.ExceptionUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.List;

/**
 * The MainScreenController class is responsible for controlling the main screen of the application.
 */
public class MainScreenController {
    private static final Integer APPLICATION_WIDTH = 1280;
    private static final Integer APPLICATION_HEGHT = 720;
    private static final ILogger LOG = LoggerFactory.createLogger(MainScreenController.class);

    private final JFrame mainScreenFrame;
    private final MainScreenModel model;

    public MainScreenController() throws IOException, ModulePathsNotSetException, HoustonConfigPathNotSetException {
        model = new MainScreenModel();
        SelectDirectoryModalController selectModulePathsController = new SelectDirectoryModalController(model, true);

        // Set paths
        setPaths(model, selectModulePathsController);

        // Load configurations
        model.loadConfigurations();
        List<ConfigurationDTO> configurations = model.getConfigurations();
        ConfigurationTableController configurationTableController = new ConfigurationTableController(model);

        // Initialize User Interface
        mainScreenFrame = initializeMainFrame(configurationTableController);

        // Add configurations to table
        for (ConfigurationDTO configuration : configurations) {
            configurationTableController.addConfigurationToTable(configuration);
        }
    }

    private void setPaths(MainScreenModel model, SelectDirectoryModalController selectModulePathsController){
        if (model.getModulePaths().isEmpty()) {
            LOG.info("Module paths not set. Open dialog");
            selectModulePathsController.showDialog();
            LOG.info("Module paths selected");
        }
        if (model.getRootHoustonConfigPath() == null) {
            LOG.info("Houston config path not set. Open dialog");
            selectModulePathsController.setMiddlewareDirectory(false);
            selectModulePathsController.showDialog();
            LOG.info("Houston config path selected");
        }
    }

    private JFrame initializeMainFrame(ConfigurationTableController configurationTableController){
        JFrame mainFrame = new JFrame("");
        mainFrame.setSize(APPLICATION_WIDTH, APPLICATION_HEGHT);
        mainFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        mainFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                onWindowClosing();
            }
        });
        mainFrame.setResizable(false);
        mainFrame.getContentPane().add(configurationTableController.getPanel());
        return mainFrame;
    }

    private void onWindowClosing() {
        try {
            model.savePathsToConfiguration();
            System.exit(0);
        } catch (IOException ex) {
            LOG.error("Error when closing app: {}", ExceptionUtils.getStackTrace(ex));
            System.exit(1);
        }
    }

    public void open() {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        mainScreenFrame.setLocation(
                dimension.width / 2 - mainScreenFrame.getSize().width / 2,
                dimension.height / 2 - mainScreenFrame.getSize().height / 2
        );

        mainScreenFrame.setVisible(true);
    }
}
