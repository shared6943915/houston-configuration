package com.madera.michal.houstonconfiguration.ui.mainscreen.controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public abstract class AbstractModalController {
    protected static final Integer PREFERRED_TEXT_FIELD_WIDTH = 150;
    protected static final Integer PREFERRED_TEXT_FIELD_HEIGHT = 20;
    protected static final Dimension PREFERRED_TEXT_FIELD_SIZE = new Dimension(PREFERRED_TEXT_FIELD_WIDTH, PREFERRED_TEXT_FIELD_HEIGHT);
    protected JDialog dialog;
    protected JButton confirmButton;

    protected void createDialog(
            final String title,
            final Integer dialogWidth,
            final Integer dialogHeight,
            final String confirmButtonText,
            final ActionListener confirmButtonListener,
            final JPanel contentPanel
    ) {
        dialog = new JDialog();
        dialog.setTitle(title);
        dialog.setSize(dialogWidth, dialogHeight);
        dialog.setResizable(false);
        dialog.setLocationRelativeTo(null);
        dialog.setModal(true);
        dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        JButton closeButon = new JButton("Close");
        closeButon.addActionListener(e -> dialog.setVisible(false));

        confirmButton = new JButton(confirmButtonText);
        confirmButton.addActionListener(confirmButtonListener);

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(closeButon);
        buttonPanel.add(confirmButton);

        dialog.add(contentPanel);

        dialog.add(buttonPanel, BorderLayout.SOUTH);
    }

    public void showDialog() {
        dialog.setVisible(true);
    }
}
