package com.madera.michal.houstonconfiguration.configuration;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * This class provides utility methods to interact with the application's configuration.
 * It allows loading and saving properties from a configuration file.
 */
public class ConfigurationFactory {
    private static final String CONFIG_DIR_NAME = System.getProperty("configDir.path", "config");
    private static final String CONFIG_FILE_PATH = System.getProperty("configFile.path", CONFIG_DIR_NAME + File.separator + "app_config.properties");

    private ConfigurationFactory() {}

    public static Properties ensureExistsAndLoadProperties() {
        File configDir = new File(CONFIG_DIR_NAME);
        if (!configDir.exists()) {
            configDir.mkdirs();
        }
        Path path = Paths.get(CONFIG_FILE_PATH);
        if (Files.notExists(path)) {
            try {
                Files.createFile(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Properties properties = new Properties();
        try (InputStream input = new FileInputStream(CONFIG_FILE_PATH)) {
            properties.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

    public static void saveProperties(Properties properties) throws IOException {
        try (OutputStream output = new FileOutputStream(CONFIG_FILE_PATH)) {
            properties.store(output, null);
        }
    }
}
