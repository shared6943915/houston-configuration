package com.madera.michal.houstonconfiguration.configuration;

/**
 * The ConfigurationKey enum represents configuration keys used to access specific configurations.
 * Each configuration key has a corresponding configuration name.
 */
public enum ConfigurationKey {
    LOGGER_METHOD("logger.method"),
    MIDDLEWARE_PATH("middleware.path"),
    HOUSTON_CONFIG_PATH("houstonConfig.path");

    private final String configName;

    ConfigurationKey(final String configName) {
        this.configName = configName;
    }

    public String getConfigName() {
        return configName;
    }
}
