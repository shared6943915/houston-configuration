package com.madera.michal.houstonconfiguration.configuration;

import java.io.IOException;
import java.util.Properties;

/**
 * The ConfigurationProducer class is responsible for producing and managing configurations.
 * It provides methods to create, modify, and retrieve configurations.
 */
public class ConfigurationProducer {
    private static ConfigurationProducer instance = null;
    private final Properties properties = ConfigurationFactory.ensureExistsAndLoadProperties();

    private ConfigurationProducer() {}

    public static ConfigurationProducer getInstance() {
        if (instance == null) {
            instance = new ConfigurationProducer();
        }
        return instance;
    }

    public String getConfigValue(final ConfigurationKey configKey) {
        Object value = properties.get(configKey.getConfigName());
        return value != null ? value.toString() : null;
    }

    public void setConfigValue(final ConfigurationKey configKey, final String newValue) throws IOException {
        properties.setProperty(configKey.getConfigName(), newValue);
        ConfigurationFactory.saveProperties(properties);
    }
}
