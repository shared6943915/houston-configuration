package com.madera.michal.houstonconfiguration.worker;

import com.madera.michal.houstonconfiguration.logger.ILogger;
import com.madera.michal.houstonconfiguration.logger.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The HoustonConfigurationReader class is responsible for reading Houston configurations from a given path.
 */
public class HoustonConfigurationReader {
    private static final List<String> EXCLUDED_FOLDER_NAMES = Arrays.asList(
            "houston-config", "MANY"
    );
    private static final ILogger LOG = LoggerFactory.createLogger(HoustonConfigurationReader.class);

    public Map<String, Properties> readHoustonConfigurations(final String houstonConfigPath) throws IOException {
        LOG.info("Read houston configurations from " + houstonConfigPath);
        Map<String, Properties> result = new HashMap<>();
        Map<String, String> houstonConfigPaths = getHoustonConfigPaths(houstonConfigPath);

        for (Map.Entry<String, String> entry : houstonConfigPaths.entrySet()) {
            String configName = entry.getKey();
            String configPath = entry.getValue() + File.separator + "javaconfig.properties";
            LOG.info("Reading configs from path {}", configPath);
            Properties properties = new Properties();
            try (InputStream input = new FileInputStream(configPath)) {
                properties.load(input);
            }
            result.put(configName, properties);
        }
        LOG.info("Found {} configuration(s)", result.size());

        return result;
    }

    private Map<String, String> getHoustonConfigPaths(final String houstonConfigPath) throws IOException {
        try (Stream<Path> paths = Files.walk(Paths.get(houstonConfigPath), 1)) {
            return paths
                    .filter(Files::isDirectory)
                    .filter(folder -> !EXCLUDED_FOLDER_NAMES.contains(folder.getFileName().toString()))
                    .collect(Collectors.toMap(folder -> folder.getFileName().toString(), folder -> folder.toAbsolutePath().toString()));
        }
    }
}
