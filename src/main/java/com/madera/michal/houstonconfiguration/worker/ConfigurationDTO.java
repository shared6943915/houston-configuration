package com.madera.michal.houstonconfiguration.worker;

import java.util.List;

/**
 * ConfigurationDTO represents a configuration parameter with its properties and values.
 */
public class ConfigurationDTO {
    private final ConfigurationMapKey mapKey;

    private final String documentation;

    private final String type;

    private String pattern;

    private List<String> possibleValues;

    private String defaultValue;

    private List<String> defaultValueList;

    private String currentValue;

    public ConfigurationDTO(
            final ConfigurationMapKey mapKey,
            final String documentation,
            final String type
    ) {
        this.mapKey = mapKey;
        this.documentation = documentation;
        this.type = type;
    }

    public String getKey() {
        return mapKey.key();
    }

    public String getModuleName() {
        return mapKey.moduleName();
    }

    public String getDocumentation() {
        return documentation;
    }

    public String getType() {
        return type;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(final String pattern) {
        this.pattern = pattern;
    }

    public List<String> getPossibleValues() {
        return possibleValues;
    }

    public void setPossibleValues(final List<String> possibleValues) {
        this.possibleValues = possibleValues;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(final String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public List<String> getDefaultValueList() {
        return defaultValueList;
    }

    public void setDefaultValueList(final List<String> defaultValueList) {
        this.defaultValueList = defaultValueList;
    }

    public String getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(final String currentValue) {
        this.currentValue = currentValue;
    }

    public Object[] getTableRowData() {
        return new Object[] {getKey(), documentation, type, pattern, possibleValues, defaultValue, defaultValueList, currentValue};
    }

    @Override
    public String toString() {
        return "ConfigurationDTO{" +
                "mapKey=" + mapKey +
                ", documentation='" + documentation + '\'' +
                ", type='" + type + '\'' +
                ", pattern='" + pattern + '\'' +
                ", possibleValues=" + possibleValues +
                ", defaultValue='" + defaultValue + '\'' +
                ", defaultValueList=" + defaultValueList +
                ", currentValue='" + currentValue + '\'' +
                '}';
    }
}
