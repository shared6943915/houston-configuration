package com.madera.michal.houstonconfiguration.worker;

import com.madera.michal.houstonconfiguration.logger.ILogger;
import com.madera.michal.houstonconfiguration.logger.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * The ConfigDescriptorReader class is responsible for reading a configuration descriptor file
 * and retrieving its properties.
 */
public class ConfigDescriptorReader {
    private static final String CONFIG_ITEMS_KEY = "configItems";
    private static final String KEY = "key";
    private static final String DOCUMENTATION = "doc";
    private static final String TYPE = "type";
    private static final String PATTERN = "pattern";
    private static final String POSSIBLE_VALUES = "possibleValues";
    private static final String DEFAULT_VALUE = "defaultValue";

    private static final List<String> EXCLUDED_CONFIG_KEYS = Arrays.asList(
            "build.timestamp", "build.version"
    );
    private static final ILogger LOG = LoggerFactory.createLogger(ConfigDescriptorReader.class);

    public Map<ConfigurationMapKey, ConfigurationDTO> getConfigurations(final String configDescriptorPath) throws IOException {
        LOG.info("Get configurations from path {}", configDescriptorPath);
        String moduleName = getModuleDirectoryName(configDescriptorPath);

        FileInputStream fileInputStream = new FileInputStream(configDescriptorPath);
        Yaml yaml = new Yaml();

        Map<String, Object> configs = yaml.load(fileInputStream);

        List<LinkedHashMap<String, Object>> configItems = (List<LinkedHashMap<String, Object>>) configs.get(CONFIG_ITEMS_KEY);
        Map<ConfigurationMapKey, ConfigurationDTO> result = new HashMap<>();
        for (LinkedHashMap<String, Object> configItem : configItems) {
            String key = configItem.get(KEY).toString();
            if (EXCLUDED_CONFIG_KEYS.contains(key)) {
                continue;
            }
            ConfigurationMapKey mapKey = new ConfigurationMapKey(key, moduleName);

            String documentation = configItem.get(DOCUMENTATION).toString();
            String type = configItem.get(TYPE).toString();
            ConfigurationDTO configuration = new ConfigurationDTO(mapKey, documentation, type);
            Object pattern = configItem.get(PATTERN);
            if (pattern != null) {
                configuration.setPattern(pattern.toString());
            }

            List<String> possibleValues = (List<String>) configItem.get(POSSIBLE_VALUES);
            if (possibleValues != null) {
                configuration.setPossibleValues(possibleValues);
            }

            Object defaultValue = configItem.get(DEFAULT_VALUE);
            if (defaultValue != null) {
                if (defaultValue instanceof String) {
                    configuration.setDefaultValue(defaultValue.toString());
                } else if (defaultValue instanceof List) {
                    configuration.setDefaultValueList((List<String>) defaultValue);
                }
            }
            result.put(mapKey, configuration);
        }
        LOG.info("Got {} configuration(s)", result.size());

        return result;
    }

    private String getModuleDirectoryName(final String configDescriptorPath) {
        LOG.info("Get module directory name from path {}", configDescriptorPath);
        Path path = Paths.get(configDescriptorPath);
        String result = path.getName(path.getNameCount() - 5).toString();

        LOG.info("Module directory name {}", result);

        return result;
    }
}
