package com.madera.michal.houstonconfiguration.worker;

/**
 * The ConfigurationMapKey class represents a key used to store configurations in a map.
 */
public record ConfigurationMapKey(String key, String moduleName) {

    @Override
    public String toString() {
        return "ConfigurationMapKey{" +
                "key='" + key + '\'' +
                ", moduleName='" + moduleName + '\'' +
                '}';
    }
}
