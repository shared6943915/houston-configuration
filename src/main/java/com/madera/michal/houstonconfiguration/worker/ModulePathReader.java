package com.madera.michal.houstonconfiguration.worker;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * The ModulePathReader class is responsible for reading the module path from a given file.
 */
public class ModulePathReader {
    private static final List<String> EXCLUDED_FOLDER_NAMES = Arrays.asList(
            "middleware", ".git", ".githooks", ".idea", "mysql", "src", "target", "bbp", "shared"
    );

    private static ModulePathReader instance = null;

    private ModulePathReader() {}

    public static ModulePathReader getInstance() {
        if (instance == null) {
            instance = new ModulePathReader();
        }

        return instance;
    }

    public List<String> getModulePaths(final String middlewarePath) throws IOException {
        try (Stream<Path> paths = Files.walk(Paths.get(middlewarePath), 1)) {
            return paths
                    .filter(Files::isDirectory)
                    .filter(folder -> !EXCLUDED_FOLDER_NAMES.contains(folder.getFileName().toString()))
                    .map(folder -> folder.toAbsolutePath().toString())
                    .toList();
        }
    }
}
