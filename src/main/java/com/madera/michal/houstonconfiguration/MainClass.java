package com.madera.michal.houstonconfiguration;

import com.madera.michal.houstonconfiguration.ui.mainscreen.controller.MainScreenController;
import com.madera.michal.houstonconfiguration.ui.mainscreen.model.HoustonConfigPathNotSetException;
import com.madera.michal.houstonconfiguration.ui.mainscreen.model.ModulePathsNotSetException;

import java.io.IOException;

public class MainClass {
    public static void main(String[] args) {
        try {
            MainScreenController controller = new MainScreenController();
            controller.open();
        } catch (IOException | ModulePathsNotSetException | HoustonConfigPathNotSetException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
