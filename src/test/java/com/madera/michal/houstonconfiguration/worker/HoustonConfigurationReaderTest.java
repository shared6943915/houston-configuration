package com.madera.michal.houstonconfiguration.worker;

import com.madera.michal.houstonconfiguration.ui.mainscreen.model.MainScreenModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for the {@link HoustonConfigurationReader}
 */
class HoustonConfigurationReaderTest {
    private File houstonConfigMainDir;

    private HoustonConfigurationReader reader;

    @BeforeEach
    void setUp() throws Exception {
        houstonConfigMainDir = new File(Objects.requireNonNull(MainScreenModel.class.getResource("/houston-config")).toURI());

        reader = new HoustonConfigurationReader();
    }

    @Test
    void readHoustonConfigurations() throws Exception {
        Map<String, Properties> configurations = reader.readHoustonConfigurations(houstonConfigMainDir.getAbsolutePath());

        assertEquals(4, configurations.size());
    }
}
