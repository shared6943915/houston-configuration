package com.madera.michal.houstonconfiguration.worker;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test class for the {@link ModulePathReader}
 */
class ModulePathReaderTest {
    private String testProjectPath;

    private ModulePathReader modulePathReader;

    @BeforeEach
    void setUp() throws Exception {
        File f = new File(Objects.requireNonNull(ModulePathReaderTest.class.getResource("/middleware")).toURI());
        testProjectPath = f.getAbsolutePath();

        modulePathReader = ModulePathReader.getInstance();
    }

    @Test
    void getModulePaths() throws Exception {
        List<String> expectedModulePaths = Arrays.asList(
                testProjectPath + File.separator + "module1",
                testProjectPath + File.separator + "module2",
                testProjectPath + File.separator + "module3"
        );

        List<String> result = modulePathReader.getModulePaths(testProjectPath);

        assertEquals(expectedModulePaths.size(), result.size());
        for (String expectedModulePath : expectedModulePaths) {
            assertTrue(result.contains(expectedModulePath));
        }
    }

}
