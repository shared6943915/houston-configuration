package com.madera.michal.houstonconfiguration.ui.mainscreen;

import com.madera.michal.houstonconfiguration.ui.mainscreen.model.HoustonConfigPathNotSetException;
import com.madera.michal.houstonconfiguration.ui.mainscreen.model.MainScreenModel;
import com.madera.michal.houstonconfiguration.ui.mainscreen.model.ModulePathsNotSetException;
import com.madera.michal.houstonconfiguration.worker.ConfigurationDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Test class for the {@link MainScreenModel}
 */
class MainScreenModelTest {
    private MainScreenModel model;

    private List<String> modulePaths;
    private static final List<String> EXCLUDED_FOLDER_NAMES = Arrays.asList(
            "middleware", ".git", ".githooks", ".idea", "mysql", "src", "target", "shared"
    );
    private File middlewareMainDir;
    private File houstonConfigMainDir;

    @BeforeEach
    void setUp() throws Exception {
        middlewareMainDir = new File(Objects.requireNonNull(MainScreenModelTest.class.getResource("/middleware")).toURI());
        houstonConfigMainDir = new File(Objects.requireNonNull(MainScreenModel.class.getResource("/houston-config")).toURI());
        modulePaths = Arrays.asList(middlewareMainDir.listFiles())
                .stream()
                .filter(file -> file.isDirectory() && !EXCLUDED_FOLDER_NAMES.contains(file.getName()))
                .map(File::getAbsolutePath)
                .collect(Collectors.toList());

        model = new MainScreenModel();
    }

    @Test
    void createModulePaths() throws Exception {
        assertEquals(0, model.getModulePaths().size());

        model.createModulePaths(middlewareMainDir.getAbsolutePath());

        List<String> updatedModulePaths = model.getModulePaths();
        assertEquals(modulePaths.size(), updatedModulePaths.size());
        assertEquals(modulePaths, updatedModulePaths);
    }

    @Test
    void loadConfigurations() throws Exception {
        model.createModulePaths(middlewareMainDir.getAbsolutePath());
        model.setRootHoustonConfigPath(houstonConfigMainDir.getAbsolutePath());

        model.loadConfigurations();
        List<ConfigurationDTO> configurations = model.getConfigurations();

        assertEquals(6, configurations.size());
    }

    @Test
    void loadConfigurationsShouldThrowExceptionWhenModulePathNotSet() {
        assertThrows(
                ModulePathsNotSetException.class,
                () -> model.loadConfigurations()
        );
    }

    @Test
    void loadConfigurationsShouldThrowExceptionWhenHoustonConfigPathNotSet() throws Exception {
        model.createModulePaths(middlewareMainDir.getAbsolutePath());

        assertThrows(
                HoustonConfigPathNotSetException.class,
                () -> model.loadConfigurations()
        );
    }

    @Test
    void changeConfigurationValue() throws Exception {
        model.createModulePaths(middlewareMainDir.getAbsolutePath());
        model.setRootHoustonConfigPath(houstonConfigMainDir.getAbsolutePath());
        model.loadConfigurations();
        List<ConfigurationDTO> configurations = model.getConfigurations();
        ConfigurationDTO configuration = configurations.get(0);
        File houstonConfigUrl = new File(Objects.requireNonNull(MainScreenModel.class.getResource("/houston-config/" + configuration.getModuleName() + "/javaconfig.properties")).toURI());

        model.changeConfigurationValue(configuration, "ABC");

        assertEquals("ABC", configuration.getCurrentValue());

        Properties properties = new Properties();
        try (InputStream input = new FileInputStream(houstonConfigUrl.getAbsolutePath())) {
            properties.load(input);
        }
        assertEquals("ABC", properties.get(configuration.getKey()));
    }
}
