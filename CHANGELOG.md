﻿# Changelog

## [1.2.0] - 2024-05-19
**Features:**
- Copy configuration name to clipboard when user doubleclicks on the config name

**Fixes:**
- Show longer list values correctly 
- Show text field for LIST if the pattern is null
## [1.1.1] - 2024-05-10

**Fixes:**
- Excluded some folders
## [1.1.0] - 2024-05-06
**Features:**
- Add configuration for the application

**Fixes:**
- Adding list values not working
- Change the value with type LIST

**Refactors:**
- Code refactoring
## [1.0.0] - 2024-05-05
**Features:**
- Create logging 
- Change configuration value 
- Show current configuration value 
- Show configuration details 
- Add filter to configurations table 
- Add modal to select module paths 
- Read configurations from config-descriptor files
